Interlace application initialy adapted for knotting the Belgica lettering system during a residency at FLACC.info by Pierre Marchand.
Residency with Femke Snelting, Pierre Huyghebaert, Vincent Meessen, Gijs de Heij and Julien Buneau, 12 to 16 october 2015.  
Some info about the lettering system on http://ospublish.constantvzw.org/foundry/belgica-belgika