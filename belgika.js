function belgika () {
    var defaults = {
        'gap': 22,
        'line': 2,
        'advance': 66
    };

    var $input = $('#input'),
        $gap = $('#gap'),
        $line = $('#line'),
        $advance = $('#advance'),
        $hpglButton = $('#hpgl-button'),
        $hpglOut = $('#hpgl-out'),
        $hpglForm = $('#hpgl-form'),
        $svgButton = $('#svg-button'),
        $svgOut = $('#svg-out'),
        $svgForm = $('#svg-form');

    $gap.val(defaults.gap);
    $line.val(defaults.line);
    $advance.val(defaults.advance);

    var previewBox = $('#preview-box');
    var $canvas = $('<canvas />'),
        canvas = $canvas.get(0);

    $canvas.attr('width', '1000');
    $canvas.attr('height', '600');

    previewBox.append($canvas);

    var intl = new Interlace();

    var updateCanvas = function (optCanvas) {
        return function () {
            var text = $input.val().trim();
            if (0 === text.length) {
                return;
            }
            var options = {
                lineWidth: parseFloat($line.val()) || defaults.line,
                baseGap: parseFloat($gap.val()) || defaults.gap,
                strokeStyle: 'rgb(0, 0, 0)',
                lineJoin: 'round',
                lineCap: 'round',
            }

            intl.setAdvance((parseFloat($advance.val()) || defaults.advance)/100)
                .setInitials(text)
                .draw(optCanvas, options);
        };
    };

    $input.on('keyup', updateCanvas(canvas));
    $gap.on('change', updateCanvas(canvas));
    $line.on('change', updateCanvas(canvas));
    $advance.on('change', updateCanvas(canvas));

    $hpglButton.on('click', function () {
        var hpgl = new HPGL($canvas.attr('width'), $canvas.attr('height'));
        updateCanvas(hpgl)();
        $hpglOut.text(hpgl.getInstructions());
    });

    $svgButton.on('click', function () {
        var svg = new SVG($canvas.attr('width'), $canvas.attr('height'));
        updateCanvas(svg)();
        $svgOut.text(svg.getInstructions());
    });
}

$(document).ready(belgika);
