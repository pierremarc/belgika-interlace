/*
* monogram/Interlace.js
*
*
* Copyright (C) Pierre Marchand <pierremarc07@gmail.com> 2015
*
*/



// Perhaps we can also move this into the interlacer?
var INTERLACE_ORDERS = [0,1];
var INTERLACE_ORDER_TOP = INTERLACE_ORDERS[1];
var INTERLACE_ORDER_BOTTOM = INTERLACE_ORDERS[0];


// to reveal start/end points (and many other beautiful things), switch DEBUG = true
var DEBUG = false;

function drawMark(ctx, p, r, ctxOptions){
    if(!DEBUG)return;
    r = r || 10;
    ctxOptions = ctxOptions || {};
    ctx.save();
ctx.beginPath();

    _.each(ctxOptions, function(args,method){
	try {
        ctx[method].apply(ctx,args);
	}
catch (err) {
ctx[method] = args;
}
    });
    ctx.translate(p.x, p.y);
        ctx.moveTo(0, -r);
        ctx.lineTo(0, r);
        ctx.moveTo(-r, 0);
        ctx.lineTo(r, 0);
        ctx.stroke();
    ctx.restore();

};

function drawRect(ctx, rect, ctxOptions){
    if(!DEBUG)return;
    ctxOptions = ctxOptions || {};
    ctx.save();
    _.each(ctxOptions, function(args,method){
        ctx[method].apply(ctx,args);
    });
    ctx.strokeRect(rect.left(), rect.top(), rect.width(), rect.height());
    ctx.restore();
};

function interlaceReverseOrder(o){
    return (o === INTERLACE_ORDER_TOP) ? INTERLACE_ORDER_BOTTOM : INTERLACE_ORDER_TOP;
};
function interlacePickOrder(){
    return INTERLACE_ORDERS[Math.round(Math.random())];
};

function stamp(o){
    if(!o.id){
        o.id = _.uniqueId();
    }
    return o;
};

function isZero(d){
    return !!(d < 0.0000001
    && d > 0.0000001);

};


function Point(data){
    stamp(this);
    data = data || {};
    this.x = data.x || 0;
    this.y = data.y || 0;

//         _.extend(this, _.pick(data, 'x', 'y'));

//         this.__defineGetter__("x", function(){
//             return _x;
//         });
//         this.__defineSetter__("x", function(val){
//             _x = val;
//         });
//         this.__defineGetter__("y", function(){
//             return _y;
//         });
//         this.__defineSetter__("y", function(val){
//             _y = val;
//         });
}

_.extend(Point.prototype, {
    _equalPrecision: 0.01,

    transform:function(t){
        t.mapPoint(this);
        return this;
    },

    toString: function(){
        return '('+this.x+',' +this.y+')'
    },

    cross: function(p){
        return (this.x * p.y) - (this.y * p.x);
    },

    dot: function(p){
        return (this.x * p.x) + (this.y * p.y);
    },

    sub: function(p){
        return new Point({
            x: this.x - p.x,
            y: this.y - p.y
        });
    },

    add: function(p){
        return new Point({
            x: this.x + p.x,
            y: this.y + p.y
        });
    },

    distance:function(p){
        var dx = p.x - this.x;
        var dy = p.y - this.y;
        return Math.sqrt((dx*dx)+(dy*dy));
    },

    equals:function(p){
        var manhattan = Math.abs(this.x - p.x) + Math.abs(this.y - p.y);
        if(manhattan < this._equalPrecision)
        {
            return true;
        }
        return false;
    },

    lerp: function(p, t){
        var rx = this.x + (p.x - this.x) * t;
        var ry = this.y + (p.y - this.y) * t;
        return new Point({x:rx, y:ry});
    },
});

function Bezier(data){
    stamp(this);
//         // console.log('Bezier', this.id, _.keys(data) );
    this.points = {};
    _.each(data.points,
            function(p, k){
                this.points[k] = new Point(p);
            }, this);

    if(data.length){
        this._cachedLength = data.length;
    }

    if(data.segments){
        this._cachedLines = [];
        var sl = data.segments.length - 1;
        var tot = data.segments.length;
        for(var i = 0; i < sl; i++){
            var t = i / tot;
            this._cachedLines.push(new Line({
                                    points: {
                                        start: data.segments[i],
                                        end: data.segments[i+1]
                                    },
                                    parent: this,
                                    originT:t}));
        }
    }
    else{
        this.toLines();
    }
};

_.extend(Bezier.prototype, {
    clone: function(){
        return new Bezier(this.toJSON());
    },

    toJSON: function(){
        return {
                type:'C',
                points:{
                    start: this.points.start,
                    end: this.points.end,
                    control1: this.points.control1,
                    control2: this.points.control2
                    },
                length: this.getLength()
                };
    },

    at:function(t){
        if(isZero(t)){
            return new Point(this.points.start);
        }
        var d = this.points;
        // var q0 = d.start.lerp(d.control1, t);
        // var q1 = d.control1.lerp(d.control2, t);
        // var q2 = d.control2.lerp(d.end, t);
        // var r0 = q0.lerp(q1, t);
        // var r1 = q1.lerp(q2, t);
        // var b = r0.lerp(r1,t);

        // return b;

        // var p1x = d.start.x, p1y = d.start.y,
        //     c1x = d.control1.x, c1y = d.control1.y,
        //     c2x = d.control2.x, c2y = d.control2.y,
        //     p2x = d.end.x, p2y = d.end.y,
        //     x, y;

        // Implementation taken from paper.js

        var cx = 3 * (d.control1.x - d.start.x),
            bx = 3 * (d.control2.x - d.control1.x) - cx,
            ax = d.end.x - d.start.x - cx - bx,

            cy = 3 * (d.control1.y - d.start.y),
            by = 3 * (d.control2.y - d.control1.y) - cy,
            ay = d.end.y - d.start.y - cy - by;

        var x = ((ax * t + bx) * t + cx) * t + d.start.x;
        var y = ((ay * t + by) * t + cy) * t + d.start.y;

        // var p = this.points;

        // if (!this.cy) {

        //     this.cx = 3 * (p.control1.x - p.start.x),
        //     this.bx = 3 * (p.control2.x - p.control1.x) - this.cx,
        //     this.ax = p.end.x - p.start.x - this.cx - this.bx,

        //     this.cy = 3 * (p.control1.y - p.start.y),
        //     this.by = 3 * (p.control2.y - p.control1.y) - this.cy,
        //     this.ay = p.end.y - p.start.y - this.cy - this.by;
        // }

        // x = ((this.ax * t + this.bx) * t + this.cx) * t + p.start.x;
        // y = ((this.ay * t + this.by) * t + this.cy) * t + p.start.y;
        // if (!this.cy) {
        //     this._calcCurveConstants ();
        // }

        // var x = this.ax + 6 * t * this.bx + 3 * Math.pow (t, 2) * this.cx;
        // var y = this.ay + 6 * t * this.by + 3 * Math.pow (t, 2) * this.cy;

        return new Point ({x: x, y: y});
    },

    findT:function(p, center, interval){
        var inc = 1 / 100;
        var result = 10000000;
        var ret = 0;
        _.each(_.range(center-interval, center+interval,inc), function(t){
            var d = p.distance(this.at(t));
            if(result > d){result = d;ret = t};
        }, this);

        return ret;
    },

    registerIntersection: function(t){
//             // console.log('Bezier.registerIntersection',this.id, t);
        if(!this.intersectionReg){this.intersectionReg = [];}
        this.intersectionReg.push(t);
        this.intersectionReg = _.uniq(this.intersectionReg).sort();

    },

    getApproxLength: function(){
        var d = this.points;
        var dx1 = d.control1.x - d.start.x;
        var dy1 = d.control1.y - d.start.y;
        var dx2 = d.control2.x - d.control1.x;
        var dy2 = d.control2.y - d.control1.y;
        var dx3 = d.end.x - d.control2.x;
        var dy3 = d.end.y - d.control2.y;

        return Math.sqrt(dx1 * dx1 + dy1 * dy1) +
                Math.sqrt(dx2 * dx2 + dy2 * dy2) +
                Math.sqrt(dx3 * dx3 + dy3 * dy3);
    },

    getLength: function(){
        if(!this._cachedLength){
            var P = Math.max(1.0, Math.floor(this.getApproxLength()));
            var tInc = 1.0/P;
            this._cachedLength = _.reduce(_.range(tInc, 1.0, tInc),
                            function(memo, t){
                                return memo + this.at(t-tInc).distance(this.at(t));
                            }, 0, this);
        }
        return this._cachedLength;
    },

    getLengthAt: function(t){
        var P = Math.floor(this.getApproxLength());
        var tInc = 1.0/P;
        return _.reduce(_.range(tInc, t, tInc),
                        function(memo, t){
                            return memo + this.at(t-tInc).distance(this.at(t));
                        }, 0, this);
    },

    /*
    *
    */
    _split: function(t, part){
        var d = this.points;

        var q0 = d.start.lerp(d.control1, t);
        var q1 = d.control1.lerp(d.control2, t);
        var q2 = d.control2.lerp(d.end, t);
        var r0 = q0.lerp(q1, t);
        var r1 = q1.lerp(q2, t);
        var b = r0.lerp(r1, t);

        if(part){
            return new Bezier({points:{
                start:b,
                control1:r1,
                control2:q2,
                end:d.end
            }});
        }
        return new Bezier({points:{
                start: d.start,
                control1: q0,
                control2: r0,
                end: b
            }});
    },

    split: function(t, r){
        t = t || 0.5;
        r = r || 0.0;
        var t0 = Math.max(0, t - r);
        var t1 = Math.min(1.0, t + r);

        return [ this._split(t0, 0) , this._split(t1, 1) ];
    },

    transform: function(t){
        this.points.start.transform(t);
        this.points.end.transform(t);
        this.points.control1.transform(t);
        this.points.control2.transform(t);
//             _.each(this._cachedLines, function(l){l.transform(t);});
        this._cachedLines = null;
//             this.toLines();
        this._cachedLength = null;
    },

    toLines: function(){
        if(!this._cachedLines){
//                 log.warn('Bezier.toLines has no cache', this.id);
            var P = Math.max(1.0, Math.floor(this.getLength()));
            console.log('toLines', this.getLength(), P);
            var tInc = 1.0/P;
            this._cachedLines = _.map(_.range(tInc, 1.0 + tInc, tInc),
                            function(t){
                                return new Line({
                                    points: {
                                        start: this.at(t-tInc),
                                        end: this.at(t)
                                    },
                                    parent: this,
                                    originT:t});
                            }, this);
        }
        return this._cachedLines;
    },

    draw: function(context, interl){
//             // console.log('DRAW',this.id);
//         context.fillText('S '+this.id, this.points.start.x + 6, this.points.start.y - 6);
        var subSegments = [];
        if(this.intersectionReg
            && this.intersectionReg.length > 0){


            var lastOffset = 0;
            _.each(this.intersectionReg, function(t, idx){
                var intersection = interl.findOrder(this, t);
                var o = this.at(t);
//             context.fillText('t='+Math.floor(t*100), o.x+10, o.y - 10);

                if(subSegments.length > 0
                    &&  intersection.order === INTERLACE_ORDER_BOTTOM){
//                         // console.log('C', this.id);
                drawMark(context, this.at(t), 40, {setStrokeColor:[1,0,1,1], setLineWidth:[1]});


                    var mainSeg = subSegments.pop();
                    var baseR = (((interl.base_gap * intersection.gap) / this.getLength()) /2);
                    var mainT = (t - lastOffset) / (1.0 - lastOffset) ;


                    var r = ((interl.base_gap * intersection.gap) / mainSeg.getLength()) /2;
                    var subs = mainSeg.split(mainT, r);
                    lastOffset = t + baseR;
                    subSegments = subSegments.concat(subs);



                }
                else{
                    if(intersection.order === INTERLACE_ORDER_BOTTOM){
//                         // console.log('B', this.id);
  //              drawMark(context, this.at(t), 20, {setStrokeColor:[1,0,1,1], setLineWidth:[1]});
                        var r = ((interl.base_gap * intersection.gap) / this.getLength()) / 2;
                        var subs = this.split(t, r);
                        subSegments = subSegments.concat(subs);
                        lastOffset = t + r;
//                 drawMark(context, subs[0].points.end, 10, {setStrokeColor:[1,0,1,1], setLineWidth:[1]});
//                 drawMark(context, subs[1].points.start, 15, {setStrokeColor:[1,0,1,1], setLineWidth:[1]});

                    }
                    else if(subSegments.length === 0){
//                     // console.log('A', this.id);
                        subSegments.push(this);
                    }
                }


            }, this);

        }
        else{
            subSegments.push(this);
        }
//             // console.log(this.id, subSegments.length, _.reduce(subSegments, function(a,s){a.push(s.id);return a;}, []));
        context.save();
        context.beginPath();
        _.each(subSegments, function(s){
            context.moveTo(s.points.start.x, s.points.start.y);
            context.bezierCurveTo(
                            s.points.control1.x, s.points.control1.y,
                            s.points.control2.x, s.points.control2.y,
                            s.points.end.x, s.points.end.y
                            );
        });
        context.stroke();
        context.restore();

    },
});

function Line(data){
    stamp(this);
    this.points = {
        start: new Point(data.points.start),
        end: new Point(data.points.end),
    };
    this.originT = data.originT || 0;
    this.parent = data.parent;
};

function makeLine(s,e){
    return new Line({points:{
        start:{x:s[0], y:s[1]},
        end:{x:e[0], y:e[1]},
    }});
}

_.extend(Line.prototype, {

    clone: function(){
        return new Line(this.toJSON());
    },

    boundingBox: function(){
        var min = this.min();
        var max = this.max();
        return new Geom.Rect(min.x, min.y, max.x-min.x, max.y-min.y);
    },

    toString: function(){
        return '['+this.points.start.toString()+','+this.points.end.toString()+']';
    },

    toJSON: function(){
        return {
                type: 'L',
                points:{
                    start: this.points.start,
                    end: this.points.end
                    }
                };
    },

    at:function(t){
        var d = this.points;
        var b = d.start.lerp(d.end, t);
        return b;
    },

    getLength: function(){
        return this.points.start.distance(this.points.end);
    },

    split: function(t, r){
        var d = this.points;
        t = t || .5;
        r = r || .0;

        var b0 = d.start.lerp(d.end, Math.max(0, t - r));
        var b1 = d.start.lerp(d.end, Math.min(1.0, t + r));

        var l0 = new Line({points:{ start: d.start, end: b0}});
        var l1 = new Line({points:{ start: b1, end: d.end}});
        return [l0, l1];
    },

    transform: function(t){
        this.points.start.transform(t);
        this.points.end.transform(t);
    },

    min: function(){
        return new Point({
            x:Math.min(this.points.start.x, this.points.end.x),
            y:Math.min(this.points.start.y, this.points.end.y),
        });
    },

    max: function(){
        return new Point({
            x:Math.max(this.points.start.x, this.points.end.x),
            y:Math.max(this.points.start.y, this.points.end.y),
        });
    },

    toLines:function(){
        return [new Line({
                    points:{
                        start: this.points.start,
                        end: this.points.end
                    },
                    parent:this,
                    originT:0
                })];
    },

    registerIntersection: function(t){
//             // console.log('Line.registerIntersection',this.id, t);
        if(!this.intersectionReg){this.intersectionReg = [];}
        this.intersectionReg.push(t);
        this.intersectionReg = _.uniq(this.intersectionReg).sort();
    },

    intersects: function(l){

        var p = this.points.start;
        var q = l.points.start;
        var r = this.points.end.sub(p);
        var s = l.points.end.sub(q);

        var tmpQPSub = q.sub(p);
        var uNumerator = tmpQPSub.cross(r);
        var denominator = r.cross(s);

        if (isZero(uNumerator)
            && isZero(denominator)) {
            return false;
            // TODO
                // colinear, so do they overlap?
//                 return ((q.x - p.x < 0) != (q.x - p2.x < 0) != (q2.x - p.x < 0) != (q2.x - p2.x < 0)) ||
//                         ((q.y - p.y < 0) != (q.y - p2.y < 0) != (q2.y - p.y < 0) != (q2.y - p2.y < 0));
        }

        if (isZero(denominator)) {
                // lines are paralell
                return false;
        }

        var u = uNumerator / denominator;
        var t = tmpQPSub.cross(s) / denominator;

        var result =  (t >= 0) && (t <= 1) && (u >= 0) && (u <= 1);
        if(result){
            return {u:u,t:t};
        }
        return result;
    },

    angle: function(){
        var d = this.points.end.sub(this.points.start);
        var theta = Math.atan2(-d.y, d.x) * 360.0 / 6.2831853071795;
        var theta_normalized = theta < 0 ? theta + 360 : theta;
        if(theta_normalized > 360){
            return 0;
        }
        return theta_normalized;
    },

    angleTo: function(l){
        return this.angle() - l.angle();
    },

    draw: function(context, interl){
//         // console.log('DRAW',this.id);
        var subSegments = [];                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   ;

//         context.fillText('S.'+this.id, this.points.start.x + 10, this.points.start.y - 10);
        if(this.intersectionReg
            && this.intersectionReg.length > 0){

            var lastOffset = 0;
            _.each(this.intersectionReg, function(t, idx){
                var intersection = interl.findOrder(this, t);
                var origT  = t;
                t = t;
//                     // console.log('T', lastOffset, t, origT);
//                 drawMark(context, this.at(t), 20, {setStrokeColor:[1,0,1,1], setLineWidth:[1]});
                var o = this.at(t);
//             context.fillText('t='+Math.floor(t*100), o.x+5, o.y - 5);

                if(subSegments.length > 0
                    &&  intersection.order === INTERLACE_ORDER_BOTTOM){
       //         drawMark(context, this.at(t), 20, {setStrokeColor:[1,0,1,1], setLineWidth:[1]});


                    var mainSeg = subSegments.pop();
                    var baseR = (((interl.base_gap * intersection.gap) / this.getLength()) /2);
                    var mainT = (t - lastOffset) / (1.0 - lastOffset);

                    var r = ((interl.base_gap * intersection.gap) / mainSeg.getLength()) / 2;
                    var subs = mainSeg.split(mainT, r);

                    var dbg = this.at(lastOffset);
//                     context.fillText('o='+Math.floor(lastOffset*100), dbg.x, dbg.y - 10);
//                     drawMark(context, this.at(lastOffset), 10, {setStrokeColor:[1,0,0,1], setLineWidth:[1]});
//                     drawMark(context,  mainSeg.at(mainT), 20, {setStrokeColor:[1,0,0,1], setLineWidth:[1]});

                    lastOffset = t + baseR;
                    subSegments = subSegments.concat(subs);



                }
                else{
                    if(intersection.order === INTERLACE_ORDER_BOTTOM){
    //            drawMark(context, this.at(t), 20, {setStrokeColor:[1,0,1,1], setLineWidth:[1]});
//                         context.fillText('t='+Math.floor(t*100), o.x+10, o.y - 10);
                        var r = ((interl.base_gap * intersection.gap) / this.getLength()) / 2;
                        var subs = this.split(t, r);
                        subSegments = subSegments.concat(subs);
                        lastOffset = t + r;
//                         drawMark(context, subs[1].points.start, 30, {setStrokeColor:[1,0,0,1], setLineWidth:[1]});
//                         drawMark(context, subs[0].points.end, 20, {setStrokeColor:[1,0,1,1], setLineWidth:[1]});

                    }
                    else if(subSegments.length === 0){
                        subSegments.push(this);
                    }
                }
            }, this);
        }
        else{
            subSegments.push(this);
        }
//         // console.log('SEG', this.intersectionReg ? this.intersectionReg.length : 0, subSegments.length);
        context.save();
        context.beginPath();
        var lastEnd = undefined;
        _.each(subSegments, function(s){
            if(!lastEnd || !lastEnd.equals(s.points.start)){
                context.moveTo(s.points.start.x, s.points.start.y);
            }
            context.lineTo(s.points.end.x, s.points.end.y);
        });
        context.stroke();
        context.restore();
    },
});


function makeLine(s,e){
    return new Line({points:{
        start:{x:s[0], y:s[1]},
        end:{x:e[0], y:e[1]},
    }});
}

function Path(data){
    stamp(this);
    this.segments = [];

    if(data){
        _.each(data, function(d){
            this.addSegment(d);
        }, this);
    }


};

_.extend(Path.prototype, {

    addSegment: function(sd){
        if(sd.type === 'C'){
            var s = new Bezier(sd);
            s.path = this;
            this.segments.push(s);
        }
        else if(sd.type === 'L'){
            var s =new Line(sd);
            s.path = this;
            this.segments.push(s);
        }
        this._length = _.reduce(this.segments, function(memo, s){ return memo + s.getLength(); }, 0);
        this._lengths = _.reduce(this.segments,
                                function(memo, s){
                                    var l = memo[memo.length - 1][0];
                                    memo.push([s.getLength()+l , s]);
                                    return memo;
                                    }, [[0,null]]);
    },

    at: function(t){
        var st = t * this._length;
        var s = undefined;
        var startingPoint = 0;
        for(var i=0; i<this._lengths.length; i++){
            if(st < this._lengths[i][0]){
                s = this._lengths[i][1];
                startingPoint = this._lengths[i-1][0];
                break;
            }
        }
        var localT = (st - startingPoint) / s.getLength();
        return s.at(localT);
    },

    each: function(f, ctx){
        ctx = ctx || this;
        return _.each(this.segments, f, ctx);
    },

    clone: function(){
        var np = new Path();
        _.each(this.segments, function(s){
            np.addSegment(s.toJSON());
        });
        return np;
    },

    getBoundingBox: function(){
        if(!this._bbox){
            _.each(this.toLines(), function(ls){
                if(!this._bbox){
                    this._bbox = ls.bbox;
                }
                else{
                    this._bbox.add(ls.bbox);
                }
            }, this);
        }
        if(!this._bbox){
            console.error('BBOX Ooops');
        }
        return this._bbox;
    },

    transform: function(t){
        this.each(function(s){
            s.transform(t);
        });
        this._bbox = undefined;
        return this;
    },

    translate: function(tx, ty){
        var t = new Geom.Transform();
        ty = ty || 0;
        t.translate(tx,ty);
        return this.transform(t);
    },


    scale: function(sx, sy){
        var t = new Geom.Transform();
        sy = sy || sx;
        t.scale(sx,sy);
        return this.transform(t);
    },

    toLines:function(){
        var ls = [];
        this.each(function(s){
            var lines = s.toLines();
            if(lines.length > 1){
                var pivot = Math.floor(lines.length/2);
                lines.bbox = lines.bboxLeft = _.reduce(lines.slice(0, pivot), function(memo, pl){
                            return memo.add(pl.boundingBox());
                        }, lines[0].boundingBox());
                lines.bboxRight = _.reduce(lines.slice(pivot), function(memo, pl){
                            return memo.add(pl.boundingBox());
                        }, lines[0].boundingBox());
                lines.bbox.add(lines.bboxRight);
                lines.pivot = pivot;
            }
            else
            {
                lines.bbox = lines.bboxLeft = lines.bboxRight = _.reduce(lines, function(memo, pl){
                            return memo.add(pl.boundingBox());
                        }, lines[0].boundingBox());
                lines.pivot = undefined;
            }
            ls.push(lines);
        });
        return ls;
    },

    _addItsToResult: function(it, tl, pl, result){
        var tLen = tl.getLength();
        var tParentLen = tl.parent.getLength();
        var t =  Math.abs((tLen / tParentLen * (it.t)));
        if(tl.originT){
            t =  Math.abs(tl.originT - (tLen / tParentLen * (1.0-it.t)));
        }


        var pLen = pl.getLength();
        var pParentLen = pl.parent.getLength();
        var u = Math.abs((pLen / pParentLen * (it.u)));
        if(pl.originT){
            u = Math.abs(pl.originT - (pLen / pParentLen * (1.0-it.u)));
        }

        var angle = tl.angleTo(pl);
        tl.parent.registerIntersection(t);
        pl.parent.registerIntersection(u);
        result.push({ s0:tl.parent, s1:pl.parent, t:t, u:u, angle:Math.abs(angle), lines:[tl,pl]});
    },

    intersections: function(p){
        this.itBoxes = [];
        var result = [];
        var self = this;
        if(this.getBoundingBox().intersects(p.getBoundingBox())){
            var time_l = Date.now();
            var plines = p.toLines();
            var tlines = self.toLines();
            //log.// console.log('Got lines in ', Date.now() - time_l, plines.length, tlines.length);
            var n_its = 0;
            var skipped_loops = 0;
            var time_i = Date.now();
            _.each(tlines, function(tls){
                _.each(tls, function(tl, tidx){
                    var tbbox = tl.boundingBox();
                    _.each(plines, function(pls){

                        if(tbbox.intersects(pls.bbox)){
                            if(0 === pls.pivot){
                                log.error('Pivot is 0, something wont work');
                            }
                            if(tbbox.intersects(pls.bboxLeft)){
                                self.itBoxes.push(tbbox);
                                var res = _.filter(pls.slice(0, pls.pivot), function(pl){
                                    var it = tl.intersects(pl);
                                    n_its++;
                                    if(it){
                                        self._addItsToResult(it, tl, pl, result);
                                        return true;
                                    }
                                    return false;
                                });
                                // console.log('Left', res.length, pls.length, pls.pivot);
                            }

                            if(pls.pivot
                                && tbbox.intersects(pls.bboxRight)){
                                self.itBoxes.push(tbbox);
                                var res = _.filter(pls.slice(pls.pivot), function(pl){
                                    var it = tl.intersects(pl);
                                    n_its++;
                                    if(it){
                                        self._addItsToResult(it, tl, pl, result);
                                        return true;
                                    }
                                    return false;
                                });
                                // console.log('Right', res.length, pls.length, pls.pivot);
                            }

                        }
                        else{
                            skipped_loops++;
                        }
                    });
                });
            });
            // console.log('N', Date.now() - time_i, n_its, skipped_loops);
        }
        return result;
    },

    draw: function(context, interl){
        this.each(function(s){
            s.draw(context, interl);
        })
    },
});

function getLinesLen(ls){
    return _.reduce(ls, function(memo, l){
        return memo + l.getLength();
    }, 0);
}

function interlacer(intersections){
    this._order = [];
    this.base_gap_unscaled = 76;
    this.base_gap = 48;
    this.base_stroke_width_unscaled = 20;
    this.base_stroke_width = 5;

    _.each(intersections, function(it){

        // we first loopup the list to see if they already crossed
        var ts = _.filter(this._order, function(o){
                return (o[0].segment.path.id === it.s0.path.id )
                    && (o[1].segment.path.id === it.s1.path.id);
            });

        var r = this.adjustGap(it.angle);

        if(ts.length > 0){
//             // console.log('GOT A LAST CROSS', it.s0.id, it.s1.id);
            var lastCross = _.last(ts);
            this._order.push([{segment:it.s0, t:it.t, order:lastCross[1].order, gap:r},
                                {segment:it.s1, t:it.u, order:lastCross[0].order, gap:r}]);
        }
        else{
            // random init
            var ordr = INTERLACE_ORDER_BOTTOM; //; interlacePickOrder()
            this._order.push([{segment:it.s0, t:it.t, order:ordr, gap:r},
                                    {segment:it.s1, t:it.u, order:interlaceReverseOrder(ordr), gap:r}]);
        }
    }, this);

//     _.each(this._order, function(o){
//         // console.log('O:', o[0].segment.id, o[1].segment.id, o[0].order === o[1].order, o[0].order, o[1].order);
//     });
};

_.extend(interlacer.prototype, {
    adjustGap: function(angle){
        var ar = angle;
        if(angle > 180) ar = ar - 180;
        var da = Math.abs(90 - (Math.abs(ar)));

        var adj = (da / 90);
        return 1 + adj;

    },

    findOrder: function(segment, t){
        var to = _.find(_.flatten(this._order), function(o){
            return (o.segment.id === segment.id && o.t === t) ;
        });
        if(!to){
            log.warn('Could not find an intersection for', segment, t);
        }
        return to;
    },
});

var Interlace = function(options){
    this.defaultDrawingOptions = {
        lineWidth: 1,
        baseGap: 4,
        strokeStyle: 'rgb(0, 0, 0)',
        lineJoin: 'round',
        lineCap: 'round',
    };

    options = options || {};
    if ('initials' in options) {
        this.setInitials(options.initials);
    }
    this.advance = options.advance || 0.5;
    this.currentFont = options.font || font0;
};

_.extend(Interlace.prototype, {
    setInitials: function(initials){
        var diff = initials != this.initials;
        this.initials = initials;
        if(this.built && diff){
            this.build();
        }
        return this;
    },

    setAdvance: function (adv) {
        var diff = adv != this.advance;
        this.advance = adv;
        if(this.built && diff){
            this.build();
        }
        return this;
    },

    getGlyph: function (charCode, origin, isFirst) {
        var font = this.currentFont;
        if(!(charCode in font)){
            return [new Path(), origin];
        }

        var pth = new Path(font[charCode].glyph),
            bb = pth.getBoundingBox(),
            bbl = bb.left();
        pth.translate(-bbl, 0);
        pth.translate(origin.x, origin.y);
        bb = pth.getBoundingBox();
        if (isFirst) {
            this.baseRect = bb;
        }
        else {
            this.baseRect.add(bb, true);
        }
        var nextX = bb.left() + (bb.width() * this.advance);
        var nextOrigin = new Point({
            x: nextX,
            y: origin.y
        });
        return [pth, nextOrigin];

    },

    build: function(){
        if(!this.initials){
            throw (new Error('Interlace.build called without initials being set'));
        }
        this.baseRect = undefined;
        this.paths = [];

        var initials = this.initials;

        var currentAnchor = new Point();
        var isFirst = true;
        _.each(initials, function(char){
            var c = char.charCodeAt(0);
            if(c === 32){
                currentAnchor.x += 700;
            }
            else {
                var gg = this.getGlyph(c, currentAnchor, isFirst);
                this.paths.push(gg[0]);
                console.log(gg[1].x);
                currentAnchor = gg[1];
                isFirst = false;
            }
        }, this);

        this.its = [];
        var control = {};

        _.each(this.paths, function(p){
            _.each(this.paths, function(pp){
                var hach0 = p.id+'_'+pp.id;
                var hach1 = pp.id+'_'+p.id;
                if(!(hach0 in control))
                {
                    control[hach0] = true;
                    control[hach1] = true;
                    if(p.id !== pp.id){
                        this.its = this.its.concat(p.intersections(pp));
                    }
                }
            }, this);
        }, this);

        this.built = true;
        return this;
    },

    draw: function(canvas, options){
        if(!this.built) this.build();

        var context = canvas.getContext('2d');
        var baseRect = new Geom.Rect(this.baseRect);
        var canvasRect = new Geom.Rect(canvas.width * 0.1, canvas.height * 0.1,
                                       canvas.width * 0.8, canvas.height * 0.8);
        var T = baseRect.fitRect(canvasRect);
        var itrlcr = new interlacer(this.its);
        var options = (typeof(options) == 'object') ? options : {};

        options = _.defaults(options || {}, this.defaultDrawingOptions);

        context.clearRect(0, 0, canvas.width, canvas.height);

        itrlcr.stroke_width = options.lineWidth;
        itrlcr.base_gap = options.baseGap;

        context.save();

        _.each(_.omit(options, ['baseGap']), function (value, key) {
            context[key] = value;
        }, this);

        _.each(this.paths, function(original_path){
            var p = original_path;
            p.transform(T);
            p.draw(context, itrlcr);
            p.transform(T.inverse());
        }, this);

        context.restore();
    },
});
