

function MaF (val) {
    return Math.floor(val);
}




function HPGL (width, height) {
    this.plotterWidth = 420  * 40;
    this.plotterHeight = 297 * 40;
    this.width = width;
    this.height = height;
    var scale = this.plotterWidth / width;
    this.transform = new Geom.Transform();
    this.transform.translate(0, -this.height);
    this.transform.scale(scale, -scale);
    // var sc = 'SC0,' + scale + ',0,-' + scale +',2';
    this.header = [ 'IN', 'SP1'];
    this.clearRect();
}

HPGL.prototype.PEN_DOWN = 0;
HPGL.prototype.PEN_UP = 1;

HPGL.prototype.transformPoint = function (x, y) {
    var p = {x:x, y:y};
    this.transform.mapPoint(p);
    return {
        x: MaF(p.x),
        y: MaF(p.y)
    };
};

HPGL.prototype.penUp = function () {
    this.penStatus = this.PEN_UP;
    return this;
};

HPGL.prototype.penDown = function () {
    this.penStatus = this.PEN_DOWN;
    return this;
};

HPGL.prototype.getContext = function () {
    return this;
};

HPGL.prototype.save = function () {
    return this;
};

HPGL.prototype.restore = function () {
    return this;
};

HPGL.prototype.beginPath = function () {
    return this;
};

HPGL.prototype.stroke = function () {
    return this;
};

HPGL.prototype.clearRect = function () {
    this.instructions = _.clone(this.header);
    this.currentX = 0;
    this.currentY = 0;
    this.penUp();
    return this;
};


HPGL.prototype.moveTo = function (x, y) {
    // x = MaF(x);
    // y = MaF(y);
    if ((x === this.currentX) && (y === this.currentY)) {
        return;
    }
    this.currentX = x;
    this.currentY = y;
    var p = this.transformPoint(x, y);
    this.instructions.push('PU' + p.x + ','+ p.y);
    this.penUp();
    return this;
};


HPGL.prototype.lineTo = function (x, y) {
    // x = MaF(x);
    // y = MaF(y);
    if ((x === this.currentX) && (y === this.currentY)) {
        return;
    }
    this.currentX = x;
    this.currentY = y;
    var p = this.transformPoint(x, y);
    if (this.PEN_UP === this.penStatus) {
        this.instructions.push('PD' + p.x + ',' + p.y);
    }
    else {
        this.instructions.push('PA' + p.x + ',' + p.y);
    }
    this.penDown();
    return this;
};

HPGL.prototype.bezierCurveTo = function (c1x, c1y, c2x, c2y, endX, endY) {
    // var cdata = {
    //     points: {
    //         control1: {x: MaF(c1x), y: MaF(c1y)},
    //         control2: {x: MaF(c2x), y: MaF(c2y)},
    //         start: {x: MaF(this.currentX), y: MaF(this.currentY)},
    //         end: {x: MaF(endX), y: MaF(endY)},
    //     }
    // };
    var cdata = {
        points: {
            control1: {x: c1x, y: c1y},
            control2: {x: c2x, y: c2y},
            start: {x: this.currentX, y: this.currentY},
            end: {x: endX, y: endY},
        }
    };

    this.currentX = MaF(endX);
    this.currentY = MaF(endY);

    var bz = new Bezier(cdata),
        intrlcr = new interlacer([]);
    _.each(bz.toLines(), function (l) {
        l.draw(this, intrlcr);
    }, this);

};


HPGL.prototype.getInstructions = function () {
    var instructions = _.clone(this.instructions);
    instructions.push('SP0');
    return instructions.join(';');
};
