

function MaF (val) {
    return Math.floor(val);
}




function SVG (width, height) {
    this.width = width;
    this.height = height;
    this.transform = new Geom.Transform();

    this.out = [
        '<?xml version="1.0" encoding="UTF-8" standalone="no"?>',
        `<svg xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg"
width="${width}mm"
height="${height}mm"
viewBox="0 0 ${width} ${height}"
version="1.1">`
    ]

    this.currentPath = '';
}

SVG.prototype.startPath = function () {
    this.endPath();
    this.currentPath = '';
};

SVG.prototype.endPath = function () {
    if (this.currentPath.length > 0) {
        this.out.push(`<path d="${this.currentPath}" style="fill:none;stroke-width:3;stroke-miterlimit:4;stroke:#000000" />`);
    }
};

SVG.prototype.addSegment = function (s) {
    // console.log(`addSegment << ${this.currentPath} ${s}`);
    var oldValue = this.currentPath;
    // if (oldValue.length > 0) {
    //     this.currentPath = oldValue + ' ' +s;
    // }
    this.currentPath = oldValue + ' ' +s;
    // console.log(`addSegment >> ${this.currentPath}`);
};

SVG.prototype.transformPoint = function (x, y) {
    var p = {x:x, y:y};
    this.transform.mapPoint(p);
    return {
        x: MaF(p.x),
        y: MaF(p.y)
    };
};

SVG.prototype.penUp = function () {
    this.penStatus = this.PEN_UP;
    return this;
};

SVG.prototype.penDown = function () {
    this.penStatus = this.PEN_DOWN;
    return this;
};

SVG.prototype.getContext = function () {
    return this;
};

SVG.prototype.save = function () {
    return this;
};

SVG.prototype.restore = function () {
    return this;
};

SVG.prototype.beginPath = function () {

    return this;
};

SVG.prototype.stroke = function () {
    this.endPath();
    return this;
};

SVG.prototype.clearRect = function () {
    this.currentX = 0;
    this.currentY = 0;
    this.penUp();
    return this;
};


SVG.prototype.moveTo = function (x, y) {
    // x = MaF(x);
    // y = MaF(y);
    if ((x === this.currentX) && (y === this.currentY)) {
        return;
    }
    this.startPath();
    this.currentX = x;
    this.currentY = y;
    var p = this.transformPoint(x, y);
    this.addSegment('M ' + p.x + ','+ p.y +' ');
    this.penUp();
    return this;
};


SVG.prototype.lineTo = function (x, y) {
    // x = MaF(x);
    // y = MaF(y);
    if ((x === this.currentX) && (y === this.currentY)) {
        return;
    }
    var p = this.transformPoint(x, y);
    if (this.currentPath.length === 0) {
        var m = this.transformPoint(this.currentX, this.currentY);
        this.addSegment( 'M ' + m.x  + ',' + m.y );
    }
    if (this.PEN_UP === this.penStatus) {
        this.addSegment( 'L ' + p.x + ',' + p.y );
    }
    else {
        this.addSegment( 'L ' + p.x + ',' + p.y );
    }
    this.currentX = x;
    this.currentY = y;
    this.penDown();
    return this;
};

SVG.prototype.bezierCurveTo = function (c1x, c1y, c2x, c2y, endX, endY) {
    // var cdata = {
    //     points: {
    //         control1: {x: MaF(c1x), y: MaF(c1y)},
    //         control2: {x: MaF(c2x), y: MaF(c2y)},
    //         start: {x: MaF(this.currentX), y: MaF(this.currentY)},
    //         end: {x: MaF(endX), y: MaF(endY)},
    //     }
    // };
    var cdata = {
        points: {
            control1: {x: c1x, y: c1y},
            control2: {x: c2x, y: c2y},
            start: {x: this.currentX, y: this.currentY},
            end: {x: endX, y: endY},
        }
    };
var c1 =this.transformPoint(c1x,c1y);
var c2 =this.transformPoint(c2x,c2y);
var end =this.transformPoint(endX,endY);

    this.addSegment(`C ${c1.x},${c1.y} ${c2.x},${c2.y} ${end.x},${end.y}`)
    //
    // this.currentX = MaF(endX);
    // this.currentY = MaF(endY);
    //
    // var bz = new Bezier(cdata),
    //     intrlcr = new interlacer([]);
    // _.each(bz.toLines(), function (l) {
    //     l.draw(this, intrlcr);
    // }, this);
};


SVG.prototype.getInstructions = function () {
    var out = _.clone(this.out);
    out.push('</svg>');
    return out.join('\n');
};
